/*
    Android Security Scanner
    Copyright (C) 2021  besendorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

package com.besendorf.androidsecurityscanner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.drm.DrmInfo;
import android.drm.DrmInfoRequest;
import android.drm.DrmManagerClient;
import android.hardware.fingerprint.FingerprintManager;
import android.media.MediaDrm;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyInfo;
import android.security.keystore.KeyProperties;
import android.security.keystore.StrongBoxUnavailableException;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;

//import androidx.biometric.BiometricPrompt;

public class MainActivity extends AppCompatActivity {

    static final UUID WIDEVINE_UUID = new UUID(0xEDEF8BA979D64ACEL, 0xA3C827DCD51D21EDL);
    private static final String TAG = "Collector";
    KeyStore ks;
    private TextView reportTextView;
    private FingerprintManager fingerprintManager;
    private PackageManager pm;
    private JSONObject json;
    private String sreport;

    static String getProp(String s) {
        // from https://bitbucket.org/oF2pks/kaltura-device-info-android/src/master/app/src/main
        // /java/com/oF2pks/kalturadeviceinfos/Utils.java licenced under GPLv3
        try {
            @SuppressLint("PrivateApi")
            Class<?> aClass = Class.forName("android.os.SystemProperties");
            Method method = aClass.getMethod("get", String.class);
            Object platform = method.invoke(null, s);

            return platform instanceof String ? (String) platform : "<" + platform + ">";

        } catch (Exception e) {
            return "null(<" + e + ">)";
        }
    }

    private static String getCpu() {
        // from https://bitbucket.org/oF2pks/kaltura-device-info-android/src/master/app/src/main
        // /java/com/oF2pks/kalturadeviceinfos/Collector.java licenced under GPLv3
        try {
            Process p = Runtime.getRuntime().exec("cat /proc/cpuinfo");
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = null;

            while ((tmp = br.readLine()) != null) {
                if (tmp.contains("Hardware")) return tmp.substring(11);
                if (tmp.contains("model name")) tmp2 = tmp.substring(13);
            }
            is.close();
            br.close();
            if (tmp2 != null) return tmp2;
            return "Unknow";
        } catch (Exception ex) {
            return "ERROR: " + ex.getMessage();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        reportTextView = (TextView) findViewById(R.id.reportTextView);
        json = new JSONObject();
        pm = getPackageManager();
    }

    public void onBtnClick(View view) {
        try {
            json.put("MANUFACTURER", Build.MANUFACTURER);
            json.put("MODEL", Build.MODEL);
            json.put("HARDWARE", Build.HARDWARE); //change to DEVICE?
            json.put("DEVICE", Build.DEVICE);
            json.put("PRODUCT", Build.PRODUCT);
            json.put("CPU_MANUFACTURER", getCpu());
            json.put("CPU", getProp("ro.board.platform"));
            json.put("FINGERPRINT", fingerprint());
            json.put("BiometricQuality", biometricQuality());
            json.put("FingerprintPM", fingerprintPM()); //checks Fingerprint reader support vie PackageManager class
            json.put("IrisPM", irisPM()); //checks Iris reader support vie PackageManager class
            json.put("MangedUsersPM", managedUsersPM()); //checks secondary user support vie PackageManager class. Secondary users use a separate encryption key and can be used for compartmentalization
            json.put("securelyDeleteUsersPM", securelyDeleteUsersPM());
            json.put("SecureLockscreenPM", secureLockscreenPM());
            json.put("ro.product.first_api_level", getProp("ro.product.first_api_level"));
            json.put("ro.boot.flash.locked", getProp("ro.boot.flash.locked"));
            json.put("ro.boot.verifiedbootstate", getProp("ro.boot.verifiedbootstate"));
            json.put("ro.boot.veritymode", getProp("ro.boot.veritymode"));
            json.put("ro.crypto.type", getProp("ro.crypto.type"));
            json.put("KeyStore", keyStorePresence());
            json.put("TEE", TEE());
            json.put("Strongbox", isStrongbox());
            json.put("StrongboxPM", isStrongboxPM());
            json.put("DeviceAdmin", deviceAdmin());
            json.put("DRM", drmInfo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            sreport = json.toString(4);
            sreport.replace("\\/", "/"); //unsure if needed
        } catch (JSONException e) {
            sreport = "{}";
        }
        reportTextView.setText(sreport);
        //write report to file
        File output = new File(this.getExternalFilesDir(null), ("report.json"));
        try {
            FileWriter writer;
            writer = new FileWriter(output);
            writer.write(sreport);
            writer.close();
            //if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q)
            Toast.makeText(this, "Wrote report to " + output, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, "Failed writing report: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private boolean securelyDeleteUsersPM() {
        return this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_SECURELY_REMOVES_USERS);
    }

    private boolean secureLockscreenPM() {
        return this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_SECURE_LOCK_SCREEN);
    }

    private boolean managedUsersPM() {
        return this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_MANAGED_USERS);
    }

    private boolean fingerprintPM() {
        return this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_FINGERPRINT);
    }

    private boolean irisPM() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return this.getPackageManager()
                    .hasSystemFeature(PackageManager.FEATURE_IRIS);
        }
        else{
            return false;
        }
    }

    private boolean deviceAdmin() {
        return this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_DEVICE_ADMIN);
    }

    private boolean isStrongboxPM() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return this.getPackageManager()
                    .hasSystemFeature(PackageManager.FEATURE_STRONGBOX_KEYSTORE);
        }
        else{
            return false;
        }
    }


    private boolean fingerprint() {
        //FingerprintManager needs Android 6.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of FingerprintManager//
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            if (fingerprintManager == null){
                return false;
            }
            return fingerprintManager.isHardwareDetected();
        }
        return false;
    }

    private String biometricQuality() {
        //Biometric need to be enrolled or else only CREDENTIAL will be returned
        if (BiometricManager.from(this).canAuthenticate(
                BiometricManager.Authenticators.BIOMETRIC_STRONG)
                == BiometricManager.BIOMETRIC_SUCCESS) {
            return "STRONG";
        } else if (BiometricManager.from(this).canAuthenticate(
                BiometricManager.Authenticators.BIOMETRIC_STRONG)
                == BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED) {
            return "BIOMETRIC_ERROR_NONE_ENROLLED";
        }
        if (BiometricManager.from(this).canAuthenticate(
                BiometricManager.Authenticators.BIOMETRIC_WEAK)
                == BiometricManager.BIOMETRIC_SUCCESS) {
            return "WEAK";
        } else if (BiometricManager.from(this).canAuthenticate(
                BiometricManager.Authenticators.BIOMETRIC_WEAK)
                == BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED) {
            return "BIOMETRIC_ERROR_NONE_ENROLLED";
        }
        if (BiometricManager.from(this).canAuthenticate(
                BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                == BiometricManager.BIOMETRIC_SUCCESS) {
            return "CREDENTIAL";
        }
        return null;
    }

    private boolean keyStorePresence() {
        // to check for KeyStore presence we try to get an Instance of KeyStore and if we get the
        // Exception we return false
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            return true;
        } catch (KeyStoreException e) {
            return false;
        }
    }

    private boolean TEE(){
        //generates a key and then uses keyInfo.isInsideSecureHardware() to check wheter it is stored in a TEE (or an SE)
        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                kg.init(new KeyGenParameterSpec.Builder("keystore1", 0)
                        .setCertificateSerialNumber(BigInteger.valueOf(1L))
                        .build());
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        SecretKey key = kg.generateKey();

        SecretKeyFactory factory = null;
        try {
            factory = SecretKeyFactory.getInstance(key.getAlgorithm(), "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
            return false;
        }
        KeyInfo keyInfo = null;
        try {
            keyInfo = (KeyInfo) factory.getKeySpec(key, KeyInfo.class);
        } catch (InvalidKeySpecException e) {
            // Not an Android KeyStore key.
            e.printStackTrace();
        }
        if (keyInfo != null){
            return keyInfo.isInsideSecureHardware();
        }
        return false;
    }

    private boolean isStrongbox() {
        // similar to keyStorePresence we use .setIsStrongBoxBacked to make using Strongbox
        // mandatory for a dummy Key Generation which will result in
        // StrongBoxUnavailableException if Strongbox is not available

        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            try {
                kg.init(new KeyGenParameterSpec.Builder("keystore1", 0)
                        .setCertificateSerialNumber(BigInteger.valueOf(1L))
                        .setIsStrongBoxBacked(true) /* Enable StrongBox */
                        .build());
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        try {
            kg.generateKey();
        } catch (StrongBoxUnavailableException e) {
            return false;
        }
        return true;


    }

    //all following DRM Code from https://bitbucket
    // .org/oF2pks/kaltura-device-info-android/src/master/app/src/main/java/com/oF2pks
    // /kalturadeviceinfos/Collector.java licenced under GPLv3
    private JSONObject drmInfo() throws JSONException {
        return new JSONObject()
                .put("drm.service.enabled", getProp("drm.service.enabled"))
                .put("modular", modularDrmInfo())
                .put("classic", classicDrmInfo());

    }

    private JSONObject classicDrmInfo() throws JSONException {
        JSONObject json = new JSONObject();

        DrmManagerClient drmManagerClient = new DrmManagerClient(this);
        String[] availableDrmEngines = drmManagerClient.getAvailableDrmEngines();

        JSONArray engines = jsonArray(availableDrmEngines);
        json.put("engines", engines);

        try {
            if (drmManagerClient.canHandle("", "video/wvm")) {
                DrmInfoRequest request = new DrmInfoRequest(DrmInfoRequest.TYPE_REGISTRATION_INFO,
                        "video/wvm");
                request.put("WVPortalKey", "OEM");
                DrmInfo response = drmManagerClient.acquireDrmInfo(request);
                String status = (String) response.get("WVDrmInfoRequestStatusKey");

                status = new String[]{"HD_SD", null, "SD"}[Integer.parseInt(status)];
                json.put("widevine",
                        new JSONObject()
                                .put("version", response.get("WVDrmInfoRequestVersionKey"))
                                .put("status", status)
                );
            }
        } catch (Exception e) {
            json.put("error", e.getMessage() + '\n' + Log.getStackTraceString(e));
        }

        drmManagerClient.release();

        return json;
    }

    private JSONArray jsonArray(String[] stringArray) {
        JSONArray jsonArray = new JSONArray();
        for (String string : stringArray) {
            if (!TextUtils.isEmpty(string)) {
                jsonArray.put(string);
            }
        }
        return jsonArray;
    }

    private JSONObject modularDrmInfo() throws JSONException {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return new JSONObject()
                    .put("widevine", widevineModularDrmInfo());
        } else {
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private JSONObject widevineModularDrmInfo() throws JSONException {
        if (!MediaDrm.isCryptoSchemeSupported(WIDEVINE_UUID)) {
            return null;
        }

        MediaDrm mediaDrm;
        try {
            mediaDrm = new MediaDrm(WIDEVINE_UUID);
        } catch (Exception e) {//UnsupportedSchemeException
            return null;
        }

        final JSONArray mediaDrmEvents = new JSONArray();

        mediaDrm.setOnEventListener(new MediaDrm.OnEventListener() {
            @Override
            public void onEvent(MediaDrm md, byte[] sessionId, int event, int extra, byte[] data) {
                try {
                    String encodedData = data == null ? null : Base64.encodeToString(data,
                            Base64.NO_WRAP);

                    mediaDrmEvents.put(new JSONObject().put("event", event).put("extra", extra).put(
                            "data", encodedData));
                } catch (JSONException e) {
                    Log.e(TAG, "JSONError", e);
                }
            }
        });

        try {
            byte[] session;
            session = mediaDrm.openSession();
            mediaDrm.closeSession(session);
        } catch (Exception e) {
            mediaDrmEvents.put(new JSONObject().put("Exception(openSession)", e.toString()));
        }


        String[] stringProps =
                {MediaDrm.PROPERTY_VENDOR, MediaDrm.PROPERTY_VERSION, MediaDrm.PROPERTY_DESCRIPTION,
                        MediaDrm.PROPERTY_ALGORITHMS, "securityLevel", "systemId", "privacyMode",
                        "sessionSharing", "usageReportingSupport", "appId", "origin", "hdcpLevel",
                        "maxHdcpLevel", "maxNumberOfSessions", "numberOfOpenSessions"};
        String[] byteArrayProps =
                {MediaDrm.PROPERTY_DEVICE_UNIQUE_ID, "provisioningUniqueId", "serviceCertificate"};

        JSONObject props = new JSONObject();

        for (String prop : stringProps) {
            String value;
            try {
                value = mediaDrm.getPropertyString(prop);
            } catch (IllegalStateException e) {
                value = "<unknown>";
            }
            props.put(prop, value);
        }
        /*for (String prop : byteArrayProps) {
            String value;
            try {
                value = Base64.encodeToString(mediaDrm.getPropertyByteArray(prop), Base64.NO_WRAP);
            } catch (IllegalStateException|NullPointerException e) {
                value = "<unknown>";
            }
            props.put(prop, value);
        }*/

        JSONObject response = new JSONObject();
        response.put("properties", props);
        response.put("events", mediaDrmEvents);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) mediaDrm.close();
        mediaDrm.release();
        return response;
    }

    public void onShareBtnClick(View view) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sreport);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}


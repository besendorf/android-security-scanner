# Android Security Scanner
This repository contains the source code for an Android app used to gather security properties from an Android device. It displayes the information in JSON layout which can then be copied/shared to a computer.

This was created during my bachelors thesis. A link to the thesis will be added here once it's finished.

# Properties
The following properties are gathered:

* device manufacterer
* model
* hardware codename
* CPU manufacterer
* CPU
* presence of a fingerprint Scanner
* prresence of a KeyStore
* Strongbox support
* the first API level
* state of bootloader (locked/unlocked)
* verified boot state

# Reports
The reports in JSON format for the devices that were tested during my bachelors thesis are available in the `data_phones` folder
